<?php
return [
    'ROLE_ADMIN' => 1,
    'ROLE_MANAGER' => 2,
    'ROLE_EMPLOYEE' => 3,
    'PERPAGE' => [10, 20, 50],
    'IS_FIRST_LOGIN' => 1,
    'IS_NOT_FIRST_LOGIN' => 0,
    'STATUS_WORK' => 1,
    'STATUS_RESIGN' => 2,
    'LIMIT_DEFAULT' => 10,
    'TYPE_GET_ALL' => 'GET_ALL',
    'TYPE_PAGINATION' => 'PAGINATION',
];
