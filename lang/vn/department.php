<?php

// sentence.php

return [
    'edit' => 'Sửa',
    'add_department' => 'Thêm phòng ban',
    'delete' => 'Xóa',
    'name' => 'Tên phòng ban',
    'description' => 'Mô tả',
    'manager' => 'Quản lý',
    'action' => 'Tác vụ',
    'select_manager' => 'Chọn quản lý',
    'save' => 'Lưu',
    'add' => 'Thêm',
    'edit_department' => 'Sửa phòng ban',
    'list_department' => 'Danh sách phòng ban',
    'close' => 'Đóng',
    'confirm_edit' => 'Bạn chắc chắn với thay đổi này ?',
    'confirm_delete' => 'Bạn có chắc chắn muốn xóa không',
];
