<?php

// sentence.php

return [
    'name' => [
        'required' => 'Thiếu tên',
    ],
    'email' => [
        'required' => 'Thiếu Email',
    ],
    'birth_day' => [
        'required' => 'Thiếu ngày sinh',
        'before' => 'Ngày sinh lớn hơn ngày hiện tại',
        'date' => 'Sai định dạng ngày',
    ],
    'start_at' => [
        'required' => 'Thiếu ngày bắt đầu',
        'before' => 'Ngày bắt đầu lớn hơn ngày hiện tại',
        'date' => 'Sai định dạng ngày',
    ],
    'status' => [
        'required' => 'Thiếu trạng thái làm việc',
        'in' => 'Sai trạng thái làm việc',
    ],
    'phone' => [
        'min' => 'Số điện thoại có độ dài 10 chữ số',
        'max' => 'Số điện thoại có độ dài 10 chữ số',
        'regex' => 'Số điện thoại gồm các chữ số',
    ],
    'department' => [
        'required' => 'Thiếu bộ phận',
        'exists' => 'Sai tên bộ phận',
    ],
    'role' => [
        'required' => 'Thiếu vị trí',
        'in' => 'Điền sai vị trí',
    ]
];
