<?php

// sentence.php

return [
    'search' => 'Search',
    'edit' => 'Edit',
    'add' => 'Add',
    'delete' => 'Delete',
    'filter' => 'Filter',
    'import' => 'Import CSV',
    'export' => 'Export CSV',
    'name' => 'Name',
    'info' => 'Info',
    'birth_day' => 'Birth Day',
    'start_at' => 'Start At',
    'status' => 'Status',
    'position' => 'Position',
    'add_employee' => 'Add Employee',
    'edit_employee' => 'Edit Employee',
    'work' => 'Working',
    'resign' => 'Resign',
    'admin' => 'Admin',
    'manager' => 'Manager',
    'employee' => 'Employee',
    'email' => 'Email',
    'department' => 'Department',
    'phone' => 'Phone',
    'action' => 'Action',
    'select_status' => 'Choose Status',
    'select_department' => 'Choose Department',
    'change_avatar' => 'Change Avatar',
    'avatar' => 'Avatar',
    'reset' => 'Reset',
    'save' => 'Save',
    'cancel' => 'Cancel',
    'list_employee' => 'List Employee',
    'view' => 'View',
    'reset_password' => 'Request new password',
    'confirm_reset' => 'Are you sure you want to reset password?',
    'confirm_delete' => 'Are you sure you want to delete?',
];
