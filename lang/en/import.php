<?php

// sentence.php

return [
    'name' => [
        'required' => 'Missing name',
    ],
    'email' => [
        'required' => 'Missing email',
    ],
    'birth_day' => [
        'required' => 'Missing birth day',
        'before' => 'The date of birth must be less than the current date',
        'date' => 'Wrong date format',
    ],
    'start_at' => [
        'required' => 'Missing start at',
        'before' => 'Working day must be less than current date',
        'date' => 'Wrong date format',
    ],
    'status' => [
        'required' => 'Missing status',
        'in' => 'Sai trạng thái làm việc',
    ],
    'phone' => [
        'min' => 'Phone number 10 digits long',
        'max' => 'Phone number 10 digits long',
        'regex' => 'Phone number consisting of digits',
    ],
    'department' => [
        'required' => 'Missing department',
        'exists' => 'Wrong department',
    ],
    'role' => [
        'required' => 'Missing position',
        'in' => 'Wrong position',
    ]
];
