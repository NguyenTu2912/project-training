<?php

// sentence.php

return [
    'edit' => 'Edit',
    'add_department' => 'Add Department',
    'delete' => 'Delete',
    'name' => 'Name',
    'description' => 'Description',
    'manager' => 'Manager',
    'action' => 'Action',
    'select_manager' => 'Select Manager',
    'save' => 'Save',
    'add' => 'Add',
    'edit_department' => 'Edit Department',
    'list_department' => 'List Department',
    'close' => 'Close',
    'confirm_edit' => 'Are you sure with this change?',
    'confirm_delete' => 'Are you sure you want to delete?',
];
