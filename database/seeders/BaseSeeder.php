<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BaseSeeder extends Seeder
{
    /**
     * Check migrated seeder
     *
     * @return bool
     */
    public function checkMigrated(): bool
    {
        $class = get_class($this);

        return (bool) DB::table('seeders')->where('class', $class)->count();
    }

    /**
     * @return void
     */
    public function insertMigrate()
    {
        $class = get_class($this);
        $now = now()->toDateTimeString();

        DB::table('seeders')->insert(['class' => $class, 'created_at' => $now]);
    }
}
