<?php

namespace Database\Seeders;

use App\Models\Department;
use Exception;
use Illuminate\Support\Facades\DB;

class InsertDepartmentsSeeder extends BaseSeeder
{
    const LENGTH = 50;

    /**
     * Seed the application's database.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {
        if ($this->checkMigrated()) {
            return;
        }

        $now = now()->toDateTimeString();
        $names = [1 => 'Hành chính', 2 => 'Kế toán', 3 => 'Tuyển dụng', 4 => 'Đào tạo'];
        $dataInserts = [];
        for ($i = 1; $i <= self::LENGTH; $i++) {
            $code = sprintf('%02d', $i);
            $dataInserts[] = [
                'name' => $names[$i] ?? "D{$code}",
                'created_at' => $now,
                'updated_at' => $now,
            ];
        }

        DB::beginTransaction();
        try {
            Department::query()->insert($dataInserts);
            $this->insertMigrate();

            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }
}
