<?php

namespace Database\Seeders;

use App\Models\Department;
use App\Models\User;
use Exception;
use Illuminate\Support\Facades\DB;

class InsertUsersSeeder extends BaseSeeder
{
    const LENGTH = 999;

    /**
     * Seed the application's database.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {
        if ($this->checkMigrated()) {
            return;
        }

        $dataInserts = [];
        $password = bcrypt('123456');
        $now = now()->toDateTimeString();
        $departments = Department::all();

        for ($i = 1; $i <= self::LENGTH; $i++) {
            $userCode = sprintf('%03d', $i);
            $name = $i === 1 ? 'Administrator' : "User {$userCode}";
            $email = $i === 1 ? 'admin@gmail.com' : "user{$userCode}@gmail.com";
            $isFirstLogin = $i === 1 ? 0 : (rand(0, 5) ? 0 : 1);
            $workingStatus = rand(0, 1) ? User::STATUS_WORKING : User::STATUS_RESIGN;
            $phone = sprintf('%010d', rand(0, 999999999));
            if ($i > 1 && $i < 31) {
                $role = User::ROLE_MANAGER;
                $departmentId = $i - 1;
            } else {
                $departmentId = $i === 1 ? 1 : $departments->random()->id;
                $role = $i === 1 ? User::ROLE_ADMIN : User::ROLE_EMPLOYEE;
            }

            $dataInserts[] = [
                'name' => $name,
                'email' => $email,
                'email_verified_at' => $now,
                'password' => $password,
                'birthday' => fake()->dateTimeBetween('-30 years', '-10 years')->format('Y-m-d'),
                'start_at' => fake()->dateTimeBetween('-1 years', '-1 days')->format('Y-m-d'),
                'status' => $workingStatus,
                'role' => $role,
                'avatar' => null,
                'phone' => $phone,
                'department_id' => $departmentId,
                'is_first_login' => $isFirstLogin,
                'created_at' => $now,
                'updated_at' => $now,
            ];
        }

        DB::beginTransaction();
        try {
            User::query()->insert($dataInserts);
            $this->insertMigrate();

            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }
}
