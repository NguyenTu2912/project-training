<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\User;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name', 50);
            $table->string('email', 100)->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password', 100);
            $table->date('birthday');
            $table->date('start_at');
            $table->rememberToken();
            $table->string('avatar', 100)->nullable();
            $table->string('phone', 20)->nullable();
            $table->bigInteger('department_id');
            $table->tinyInteger('status')->comment('working status: [1:working, 2: resign]');
            $table->tinyInteger('role')->default(User::ROLE_EMPLOYEE)->comment('[1: admin, 2:manager, 3: employee]');
            $table->tinyInteger('is_first_login')->comment('flag first login when reset password');
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
