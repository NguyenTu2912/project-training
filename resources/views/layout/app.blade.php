<!DOCTYPE html>
<html lang="vi">
<head>
    <title>
        @yield('title')
    </title>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.5/croppie.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">

    <link rel="stylesheet" href="{{ asset('/template/plugins/fontawesome-free/css/all.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('/template/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('/template/plugins/select2/css/select2.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('/template/dist/css/adminlte.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('/css/app.css') }}" />

    @yield('min-css')
</head>
<body>
    <main>
        @yield('content')
    </main>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.5/croppie.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script src="https://kit.fontawesome.com/dfbaedb840.js" crossorigin="anonymous"></script>
    <script src="{{ asset('/template/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('/template/plugins/select2/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('/template/dist/js/adminlte.min.js') }}"></script>
    <script src="{{ asset('/js/main.js') }}"></script>
    <script src="{{ asset('/js/cropper.js') }}"></script>

    <script type="text/javascript">
        var request = window.location;
        $('ul.nav-sidebar a').filter(function () {
            return this.href == request;
        }).addClass('active');

        $('ul.nav-treeview a').filter(function () {
            return this.href == request;
        }).parentsUntil(".nav-sidebar > .nav-treeview")
            .css({'display': 'block'})
            .addClass('menu-open').prev('a')
            .addClass('active');
        
        var url = "{{ route('changeLanguage') }}";

        $(".changeLang").change(function() {
            window.location.href = url + "?lang=" + $(this).val();
        });

        $(function(){
        @if(Session::has('error'))
            toastr.error("{{ Session::get('error') }}");
        @endif

        @if(Session::has('success'))
            toastr.success("{{ Session::get('success') }}");
        @endif
        });
        $(function() {
            $('.select2').select2()
        });

        @if(Auth::user() && Auth::user()->role == config('common.ROLE_EMPLOYEE'))
            $(document.body).attr('class', 'sidebar-collapse');
        @endif
    </script>
    @yield('min-script')
</body>
</html>
