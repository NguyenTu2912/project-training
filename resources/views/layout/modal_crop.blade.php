<div id="imageModel" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{ __('profile.change_avatar') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="image_demo" style="width:350px; margin-top:30px"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary crop_image">{{ __('profile.save') }}</button>
                <button type="button" class="btn btn-secondary" id="btn-close" data-dismiss="modal">
                    {{ __('profile.cancel') }}
                </button>
            </div>
        </div>
    </div>
</div>
