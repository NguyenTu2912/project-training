<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" href="#" role="button"
               @if (auth()->user()->isAdmin() || auth()->user()->isManager()) data-widget="pushmenu" @endif>
                <i class="fas fa-bars"></i>
            </a>
        </li>
    </ul>
    <ul class="navbar-nav ml-auto">
        <li class="nav-item mx-4">
            <div class="form-group" style="margin-bottom: 0%">
                <select class="nav-link form-control custom-select changeLang mx-2" id="language" name="language">
                    <option value="en" {{ session()->get('locale') == 'en' ? 'selected' : '' }}>English</option>
                    <option value="vn" {{ session()->get('locale') == 'vn' ? 'selected' : '' }}>Vietnam</option>
                </select>
            </div>
        </li>
        <li class="nav-item dropdown">
            <div class="nav-link pt-auto" style="padding-top: 3px" data-toggle="dropdown" href="#">
                <div class="user-panel d-flex">
                    <div class="image" style="cursor: pointer">
                        <img src="{{ auth()->user()->getAvatarLink() }}" class="img-circle" alt="User Image">
                    </div>
                    <div class="info">
                        <a href="#" class="text-dark">{{ auth()->user()->name }} ({{auth()->user()->department->name}})</a>
                    </div>
                </div>
            </div>
            <div class="dropdown-menu dropdown-menu-right">
                <a type="button" href="{{ route('profile.edit') }}" class="dropdown-item">
                    <i class="fas fa-user-pen mr-2"></i> {{ __('profile.edit_profile') }}
                </a>
                <a type="button" href="{{ route('changePassword') }}" class="dropdown-item">
                    <i class="fas fa-lock mr-2"></i> {{ __('profile.change_password') }}
                </a>
                <a type="button" href="{{ route('logout') }}" class="dropdown-item">
                    <i class="fas fa-sign-out mr-2"></i> {{ __('profile.logout') }}
                </a>
        </li>
    </ul>
</nav>

@if (auth()->user()->isAdmin() || auth()->user()->isManager())
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <div class="sidebar">
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                    <img src="{{ auth()->user()->getAvatarLink() }}" class="img-circle elevation-2" alt="User Image">
                </div>
                <div class="info">
                    <a href="#" class="d-block">{{ auth()->user()->name }}</a>
                </div>
            </div>
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                    data-accordion="false">
                    <li class="nav-item">
                        <a href="{{ route('profile') }}" class="nav-link">
                            <i class="nav-icon fas fa-user"></i>
                            <p>{{ __('profile.profile') }}</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-users"></i>
                            <p>
                                {{ __('profile.list_user') }}
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{ route('employees.index') }}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>{{ __('users.list_employee') }}</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('employees.create') }}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>{{ __('users.add_employee') }}</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    @if (auth()->user()->isAdmin())
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fas fa-building-user"></i>
                                <p>
                                    {{ __('profile.department') }}
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{ route('departments.index') }}" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>{{ __('department.list_department') }}</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('departments.create') }}" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>{{ __('department.add_department') }}</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </nav>
        </div>
    </aside>
@endif
