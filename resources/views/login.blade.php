@extends('layout.app')
@section('title')
    {{ __('forms.btn_title') }}
@endsection
@section('content')
{{-- @include('layout.chang_language') --}}
    <div class="hold-transition login-page">
        <div class="hold-transition login-page">
            <div class="login-box">

                <div class="card card-outline card-primary">
                    <div class="card-header text-center">
                        <a href="#" class="h1"><b>{{ __('forms.btn_title') }}</b></a>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('login') }}" method="post">
                            @csrf
                            <div class="input-group mb-3">
                                <input type="email" name="email" id="email" class="form-control" placeholder="{{ __('forms.email_title') }}">
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span class="fas fa-envelope"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="input-group mb-3">
                                <input type="password" name="password" id="password" class="form-control" placeholder="{{ __('forms.password_title') }}">
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span class="fas fa-lock"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-8">
                                    <div class="icheck-primary">
                                        <input type="checkbox" value="1" name="remember" id="remember" checked>
                                        <label for="remember">
                                            {{ __('forms.remember') }}
                                        </label>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <button type="submit" class="btn btn-primary btn-block" style="font-size: 15px">{{ __('forms.btn_title') }}</button>
                                </div>
                            </div>
                        </form>
                        <p class="mb-1">
                            <a href="{{ route('forgot_password') }}">{{ __('forms.forgot') }}</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
