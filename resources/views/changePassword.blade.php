@extends('layout.app')
@section('title')
    {{ __('profile.change_password') }}
@endsection
@section('content')
    <div class="wrapper">
        @include('layout.navbar')
        <div class="content-wrapper">
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>{{ __('profile.change_password') }}</h1>
                        </div>
                    </div>
                </div>
            </section>

            <section class="content">
                <form action="{{ route('changePasswordCustom') }}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="card card-primary">
                                <div class="card-body">
                                    <div class="form-group required">
                                        <label class="control-label"
                                            for="current_password">{{ __('profile.current_password') }}</label>
                                        <input type="password" name="current_password" id="current_password"
                                            class="form-control @if ($errors->has('current_password')) error @endif"
                                            placeholder="{{ __('profile.current_password') }}">
                                        @if ($errors->has('current_password'))
                                            <span class="text-danger">{{ $errors->first('current_password') }}</span>
                                        @endif
                                    </div>
                                    <div class="form-group required">
                                        <label class="control-label"
                                            for="new_password">{{ __('profile.new_password') }}</label>
                                        <input type="password"
                                            class="form-control @if ($errors->has('new_password')) error @endif"
                                            name="new_password" id="new_password"
                                            placeholder="{{ __('profile.new_password') }}">
                                        @if ($errors->has('new_password'))
                                            <span class="text-danger">{{ $errors->first('new_password') }}</span>
                                        @endif
                                    </div>
                                    <div class="form-group required">
                                        <label class="control-label"
                                            for="confirm_password">{{ __('profile.confirm_password') }}</label>
                                        <input type="password"
                                            class="form-control @if ($errors->has('confirm_password')) error @endif"
                                            name="confirm_password" id="confirm_password"
                                            placeholder="{{ __('profile.confirm_password') }}">
                                        @if ($errors->has('confirm_password'))
                                            <span class="text-danger">{{ $errors->first('confirm_password') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        @if (Auth::user()->is_first_login == config('common.IS_FIRST_LOGIN'))
                        <div class="col-3">
                            <button type="submit" class="btn btn-success float-right">{{ __('profile.save') }}</button>
                        </div> 
                        @else
                        <div class="col-6">
                            <a href="{{ route('profile') }}"
                                class="btn btn-secondary">{{ __('users.cancel') }}</a>
                            <button type="submit" class="btn btn-success float-right">{{ __('profile.save') }}</button>
                        </div>
                        @endif
                    </div>
                </form>
            </section>
        </div>
    </div>
@endsection
