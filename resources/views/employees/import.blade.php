@extends('layout.app')
@section('title')
    {{ __('users.import') }}
@endsection
@section('content')
    <div class="wrapper">
        @include('layout.navbar')
        <div class="content-wrapper">
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>{{ __('users.import') }}</h1>
                        </div>
                    </div>
                </div>
            </section>
            <section class="content">
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route('import-csv') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @if (count($errors) > 0)
                                <div class="row">
                                    <div class="col-md-8 col-md-offset-1">
                                        <div class="alert alert-danger alert-dismissible">
                                            @foreach ($errors->all() as $error)
                                                {{ $error }} <br>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <input type="file" name="file" class="form-control h-auto" accept=".xlsx">
                            <br>
                            <button class="btn btn-success">{{ __('users.import') }}</button>
                        </form>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
