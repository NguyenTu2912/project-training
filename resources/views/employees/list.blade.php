@extends('layout.app')
@section('title')
    {{ __('users.list_employee') }}
@endsection
@php
    $userRoles = App\Helpers\DropdownHelper::getUserRole();
    $workingStatuses = App\Helpers\DropdownHelper::getWorkingStatus();
    $paginatorLimits = App\Helpers\DropdownHelper::getPaginatorLimit();
@endphp

@section('content')
<div class="wrapper">
    @include('layout.navbar')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>{{ __('users.list_employee') }}</h1>
                    </div>
                </div>
                <form action="{{ route('search') }}" method="GET" id="search-form">
                    <div class="d-flex justify-content-center">
                        <div class="col-md-10 offset-md-1">
                            <div class="d-flex justify-content-center">
                                <div class="col-3">
                                    <div class="form-group">
                                        <label>{{ __('users.status') }}:</label>
                                        <select class="select2" style="width: 100%;" id="status" name="status">
                                            <option value="">{{ __('users.select_status') }}</option>
                                            @foreach ($workingStatuses as $status => $label)
                                                <option value="{{ $status }}"{{ request()->status == $status ? 'selected' : '' }}>
                                                    {{ $label }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <label>{{ __('users.department') }}:</label>
                                        <select class="select2" style="width: 100%;" id="department_id" name="department_id">
                                            <option value="">{{ __('users.select_department') }}</option>
                                            @foreach ($departments as $department)
                                                <option value="{{ $department->id }}"{{ request()->department_id == $department->id ? 'selected' : '' }}>
                                                    {{ $department->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group input-group-lg">
                                    <input type="search" class="form-control form-control-lg" name="keyword"
                                        placeholder="{{ __('users.search') }}" value="{{ request()->keyword }}">
                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-lg btn-default">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="limit" value="{{ request()->limit }}" />
                </form>
            </div>
        </section>
        <section class="content">
            <div class="card">
                <div class="card-header">
                    <div class="form-group" style="display: inline-block; margin-bottom: 0%;">
                        <select class="form-control custom-select item" name="limit" id="limit">
                            @foreach ($paginatorLimits as $limit)
                                <option value="{{ $limit }}" {{ request()->limit == $limit ? 'selected' : '' }}>
                                    {{ $limit }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    @if (auth()->user()->isAdmin())
                        <a href="{{ route('import') }}" class="btn btn-primary">{{ __('users.import') }}</a>
                    @endif
                    <a id="export_csv" class="btn btn-success">{{ __('users.export') }}</a>

                    <div class="card-tools">
                        @if (auth()->user()->isAdmin())
                            <a href="{{ route('employees.create') }}"
                                class="btn btn-primary">{{ __('users.add_employee') }}</a>
                        @endif
                    </div>
                </div>
                <div class="card-body p-0">
                    <table class="table table-striped projects">
                        <thead>
                            <tr>
                                <th style="width: 15%">{{ __('users.name') }}</th>
                                <th style="width: 10%">{{ __('users.department') }}</th>
                                <th style="width: 15%">{{ __('users.info') }}</th>
                                <th style="width: 8%">
                                    @sortablelink('birthday', __('users.birth_day'))
                                </th>
                                <th style="width: 8%">
                                    @sortablelink('start_at', __('users.start_at'))
                                <th style="width: 8%">{{ __('users.status') }}</th>
                                <th style="width: 8%">{{ __('users.position') }}</th>
                                @if (auth()->user()->isAdmin())
                                    <th style="width: 20%"></th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $user)
                                @php /** @var App\Models\User $user */ @endphp
                                <tr>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->department->name ?? null }}</td>
                                    <td>
                                        <div>{{ __('users.email') }}: {{ $user->email }}</div>
                                        <div>{{ __('users.phone') }}: {{ $user->phone }}</div>
                                    </td>
                                    <td>{{ Carbon\Carbon::parse($user->birthday)->format('d-m-Y') }}</td>
                                    <td>{{ Carbon\Carbon::parse($user->start_at)->format('d-m-Y') }}</td>
                                    <td>
                                        <span class="badge badge-{!! $user->isWorking() ? 'success' : 'warning' !!} rounded-pill d-inline">
                                            {{ $workingStatuses[$user->status] ?? null }}
                                        </span>
                                    </td>
                                    <td>{{ $userRoles[$user->role] ?? null }}</td>

                                    <td class="project-actions text-right">
                                        @if (auth()->user()->id !== $user->id)
                                            <a class="btn btn-primary btn-sm" href="{{ route('employees.show', $user->id) }}">
                                                <i class="fas fa-folder"></i> {{ __('users.view') }}
                                            </a>
                                        @endif
                                        @if (auth()->user()->isAdmin())
                                            <a type="button" class="btn btn-info btn-sm" href="{{ route('employees.edit', $user->id) }}">
                                                <i class="fas fa-pencil-alt"></i> {{ __('users.edit') }}
                                            </a>
                                            <a type="button" class="btn btn-danger btn-sm" {{ auth()->user()->id === $user->id ? 'disabled' : '' }}
                                                onclick="removeUser({{ $user->id }}, '{{ route('employees.destroy', $user->id) }}', '{{ __('users.confirm_delete') }}')">
                                                <i class="fas fa-trash"></i> {{ __('users.delete') }}
                                            </a>
                                            <a type="button" class="btn btn-success btn-sm"
                                                onclick="resetPassword('{{ $user->email }}', '{{ route('resetPassword') }}', '{{ __('users.confirm_reset') }}')">
                                                <i class="fas fa-pencil-alt"></i> {{ __('users.reset') }}
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card-footer d-flex justify-content-between">
                    <p style="font-size: 1.25rem" class="record">
                        @php
                            $current_page = $users->toArray()['current_page'];
                            $per_page = $users->toArray()['per_page'];
                            $total = $users->toArray()['total'];

                            $first_item = ($current_page - 1) * $per_page + 1;
                            $last_item = $current_page * $per_page > $total ? $total : $current_page * $per_page;

                            echo('Page ' . $current_page . ': ' . $first_item . '~' . $last_item . ' / ' . $total . ' ');
                        @endphp
                        {{__('users.employee')}}
                    </p>
                    {!! $users->appends(request()->except('page'))->render() !!}
                </div>
            </div>
        </section>
    </div>
</div>
@endsection

@section('min-script')
<script>
    $('select[name="limit"]').on('change', function() {
        var $searchForm = $('#search-form');
        $searchForm.find('input[name="limit"]').val(this.value);
        $searchForm.submit();
    });
</script>
@stop
