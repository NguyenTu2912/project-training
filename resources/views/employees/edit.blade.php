@extends('layout.app')
@section('title')
    {{ __('users.edit_employee') }}
@endsection
@php
    /** @var App\Models\User $user */
    $workingStatuses = App\Helpers\DropdownHelper::getWorkingStatus();
@endphp

@section('content')
<div class="wrapper">
    @include('layout.navbar')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>{{ __('users.edit_employee') }}</h1>
                    </div>
                </div>
            </div>
        </section>

        <section class="content">
            <form action="{{ route('employees.update', $user->id) }}" method="POST" enctype="multipart/form-data" id="editUser">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="col-md-6">
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">{{ __('users.edit') }}</h3>
                            </div>
                            <div class="card-body">
                                {{-- Name --}}
                                <div class="form-group required">
                                    <label class="control-label" for="name">{{ __('users.name') }}</label>
                                    <input type="text" name="name" id="name" value="{{ old('name', $user->name) }}"
                                        class="form-control {!! $errors->has('name') ? 'error' : '' !!}">
                                    @if ($errors->has('name'))
                                        <span class="text-danger">{{ $errors->first('name') }}</span>
                                    @endif
                                </div>

                                {{-- Department --}}
                                <div class="form-group required">
                                    <label class="control-label" for="department">{{ __('users.department') }}</label>
                                    <select name="department" id="department"
                                        class="form-control custom-select {!! $errors->has('department') ? 'error' : '' !!}">
                                        <option value="">{{ __('users.select_department') }}</option>
                                        @foreach ($departments as $department)
                                            <option value="{{ $department->id }}"
                                                {{ old('department', $user->department_id) == $department->id ? 'selected' : '' }}>
                                                {{ $department->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('department'))
                                        <span class="text-danger">{{ $errors->first('department') }}</span>
                                    @endif
                                </div>

                                {{-- Birthday --}}
                                <div class="form-group required">
                                    <label class="control-label" for="birthday">{{ __('users.birth_day') }}</label>
                                    <input type="date" name="birthday" id="birthday" value="{{ old('birthday', $user->birthday) }}"
                                        class="form-control {!! $errors->has('birthday') ? 'error' : '' !!}">
                                    @if ($errors->has('birthday'))
                                        <span class="text-danger">{{ $errors->first('birthday') }}</span>
                                    @endif
                                </div>

                                {{-- Working start at --}}
                                <div class="form-group required">
                                    <label class="control-label" for="start_at">{{ __('users.start_at') }}</label>
                                    <input type="date" name="start_at" id="start_at" value="{{ old('start_at', $user->start_at) }}"
                                        class="form-control {!! $errors->has('start_at') ? 'error' : '' !!}">
                                    @if ($errors->has('start_at'))
                                        <span class="text-danger">{{ $errors->first('start_at') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="card card-secondary">
                            <div class="card-body">
                                {{-- Working status --}}
                                <div class="form-group required">
                                    <label class="control-label" for="status">{{ __('users.status') }}</label>
                                    <select name="status" id="status" class="form-control custom-select {!! $errors->has('status') ? 'error' : '' !!}">
                                        @foreach ($workingStatuses as $value => $label)
                                            <option value="{!! $value !!}" {{ old('status', $user->status) == $value ? 'selected' : '' }}>
                                                {{ $label }}
                                            </option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('status'))
                                        <span class="text-danger">{{ $errors->first('status') }}</span>
                                    @endif
                                </div>

                                {{-- Avatar --}}
                                <div class="form-group">
                                    <label for="upload" class="label">{{ __('users.avatar') }}</label>
                                    <input type="file" class="" id="upload" name="image" accept="image/x-png,image/gif,image/jpeg,image/jpg" />
                                    <br>
                                    <img id="preview" src="#" alt="preview image" />
                                    <input type="text" class="" id="image_base" name="image_base" hidden />
                                    <div id="imageModel" class="modal" tabindex="-1" role="dialog">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">{{ __('users.avatar') }}</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div id="image_demo" style="width:350px; margin-top:30px"></div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-primary admin_crop_image">{{ __('users.save') }}</button>
                                                    <button type="button" class="btn btn-secondary" id="btn-close" data-dismiss="modal">
                                                        {{ __('users.cancel') }}
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                {{-- Phone --}}
                                <div class="form-group">
                                    <label for="phone">{{ __('users.phone') }}</label>
                                    <input type="text" name="phone" id="phone" value="{{ old('phone', $user->phone) }}"
                                        class="form-control {!! $errors->has('name') ? 'phone' : '' !!}">
                                    @if ($errors->has('phone'))
                                        <span class="text-danger">{{ $errors->first('phone') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <a href="{{ route('employees.index') }}" class="btn btn-secondary">{{ __('users.cancel') }}</a>
                        <button type="submit" class="btn btn-success float-right">{{ __('users.save') }}</button>
                    </div>
                </div>
            </form>
        </section>
    </div>
</div>
@endsection

@section('min-script')
    <script>
        $('#editButton').click(function(e) {
            if (($('#department').val() != {{ $user->department_id }}) && {{ $user->role }} ==
                {{ config('common.ROLE_MANAGER') }}) {
                $("#editModel").modal("show");
            } else {
                $('#editUser').submit();
            }
        });
    </script>
@stop
