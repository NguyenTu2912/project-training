@extends('layout.app')
@section('title')
    {{ __('profile.edit_profile') }}
@endsection
@section('content')
<div class="wrapper">
    @include('layout.navbar')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>{{ __('profile.edit_profile') }}</h1>
                    </div>
                </div>
            </div>
        </section>

        <section class="content">
            <form action="{{ route('profile.update') }}" method="POST">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <div class="card card-primary">
                            <div class="card-body">
                                {{-- Birthday --}}
                                <div class="form-group required">
                                    <label class="control-label" for="birthday">{{ __('profile.birth_day') }}</label>
                                    <input type="date" name="birthday" id="birthday" value="{{ old('birthday', $user->birthday) }}"
                                           class="form-control {!! $errors->has('phone') ? 'error' : '' !!}">
                                    @if ($errors->has('birthday'))
                                        <span class="text-danger">{{ $errors->first('birthday') }}</span>
                                    @endif
                                </div>

                                {{-- Phone --}}
                                <div class="form-group">
                                    <label class="control-label" for="phone">{{ __('profile.phone') }}</label>
                                    <input type="text" name="phone" id="phone" value="{{ old('phone', $user->phone) }}"
                                           class="form-control {!! $errors->has('phone') ? 'error' : '' !!}">
                                    @if ($errors->has('phone'))
                                        <span class="text-danger">{{ $errors->first('phone') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <a href="{{ route('profile') }}" class="btn btn-secondary">{{ __('users.cancel') }}</a>
                        <button type="submit" class="btn btn-success float-right">{{ __('profile.save') }}</button>
                    </div>
                </div>
            </form>
        </section>
    </div>
</div>
@stop
