@extends('layout.app')
@section('title')
    {{ __('profile.profile') }}
@endsection
@php /** @var App\Models\User $user */ @endphp

@section('content')
<div class="wrapper">
    @include('layout.navbar')
    <div class="content-wrapper">

        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>{{ __('profile.profile') }}</h1>
                    </div>
                </div>
            </div>
        </section>

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3">
                        <div class="card card-primary card-outline">
                            <div class="card-body box-profile">
                                <div class="text-center">
                                    <img class="profile-user-img img-fluid img-circle" id="image-show"
                                         src="{{ $user->getAvatarLink() }}" alt="User profile picture">
                                </div>
                                <h3 class="profile-username text-center">{{ $user->name }}</h3>
                                @if (Auth::user()->id == $user->id)
                                    <div class="d-flex justify-content-center mb-2">
                                        <form method="POST" action="" enctype="multipart/form-data">
                                            @csrf
                                            <input type="file" class="form-control" id="upload" name="image"
                                                   hidden />
                                            <label for="upload"
                                                   class="btn btn-primary">{{ __('profile.change_avatar') }}</label>
                                            @include('layout.modal_crop')
                                            <div class="upload-success">
                                            </div>
                                        </form>
                                    </div>
                                @endif

                            </div>

                        </div>
                    </div>

                    <div class="col-lg-8">
                        <div class="card mb-4">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <p class="mb-0">{{ __('profile.position') }}</p>
                                    </div>
                                    <div class="col-sm-9">
                                        @if ($user->isAdmin())
                                            <p class="text-muted mb-0">{{ __('users.admin') }}</p>
                                        @endif
                                        @if ($user->isManager())
                                            <p class="text-muted mb-0">{{ __('users.manager') }}</p>
                                        @endif
                                        @if ($user->role == config('common.ROLE_EMPLOYEE'))
                                            <p class="text-muted mb-0">{{ __('users.employee') }}</p>
                                        @endif
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <p class="mb-0">{{ __('profile.full_name') }}</p>
                                    </div>
                                    <div class="col-sm-9">
                                        <p class="text-muted mb-0">{{ $user->name }}</p>
                                    </div>
                                </div>
                                <hr>
                                @if ($user->isManager())
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <p class="mb-0">{{ __('profile.department') }}</p>
                                        </div>
                                        <div class="col-sm-9">
                                            <p class="text-muted mb-0">{{ $user->department->name }}</p>
                                        </div>
                                    </div>
                                    <hr>
                                @endif
                                <div class="row">
                                    <div class="col-sm-3">
                                        <p class="mb-0">{{ __('profile.email') }}</p>
                                    </div>
                                    <div class="col-sm-9">
                                        <p class="text-muted mb-0">{{ $user->email }}</p>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <p class="mb-0">{{ __('profile.phone') }}</p>
                                    </div>
                                    <div class="col-sm-9">
                                        <p class="text-muted mb-0">{{ $user->phone }}</p>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <p class="mb-0">{{ __('profile.birth_day') }}</p>
                                    </div>
                                    <div class="col-sm-9">
                                        <p class="text-muted mb-0">
                                            {{ Carbon\Carbon::parse($user->birthday)->format('d-m-Y') }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
@stop
