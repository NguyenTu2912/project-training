@php
    $userRoles = App\Helpers\DropdownHelper::getUserRole();
    $workingStatuses = App\Helpers\DropdownHelper::getWorkingStatus();
@endphp

<table class="table align-middle mb-0 bg-white" id="table">
    <thead class="bg-light">
    <tr>
        <th>{{ __('users.name') }}</th>
        <th>{{ __('users.email') }}</th>
        <th>{{ __('users.birth_day') }}</th>
        <th>{{ __('users.start_at') }}</th>
        <th>{{ __('users.status') }}</th>
        <th>{{ __('users.phone') }}</th>
        <th>{{ __('users.position') }}</th>
        <th>{{ __('users.department') }}</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($users as $user)
        @php /** @var App\Models\User $user */ @endphp
        <tr>
            <td>{{ $user->name }}</td>
            <td>{{ $user->email }}</td>
            <td>{{ Carbon\Carbon::parse($user->birthday)->format('d-m-Y') }}</td>
            <td>{{ Carbon\Carbon::parse($user->start_at)->format('d-m-Y') }}</td>
            <td>{{ $workingStatuses[$user->status] ?? null }}</td>
            <td>{{ $user->phone }}</td>
            <td>{{ $userRoles[$user->role] ?? null }}</td>
            <td>{{ $user->department->name ?? null }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
