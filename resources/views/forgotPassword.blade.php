@extends('layout.app')
@section('title')
    {{ __('forms.forgot') }}
@endsection
@section('content')
    <div class="hold-transition login-page">
        <div class="login-box">
            <div class="card card-outline card-primary">
                <div class="card-header text-center">
                    <a href="#" class="h2"><b>{{ __('forms.forgot') }}</b></a>
                </div>
                <div class="card-body">
                    <form action="{{ route('forgot_password') }}" method="post">
                        @csrf
                        <div class="input-group mb-3">
                            <input type="email" name="email" id="email" class="form-control" placeholder="{{ __('users.email') }}">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-envelope"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <button type="submit" class="btn btn-primary btn-block">{{ __('users.reset_password') }}</button>
                            </div>

                        </div>
                    </form>
                    <p class="mt-3 mb-1">
                        <a href="{{ route('login') }}">{{ __('forms.btn_title') }}</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection
