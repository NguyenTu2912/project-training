@extends('layout.app')
@section('title')
    {{ __('department.edit_department') }}
@endsection
@section('content')
<div class="wrapper">
    @include('layout.navbar')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>{{ __('department.edit_department') }}</h1>
                    </div>
                </div>
            </div>
        </section>

        <section class="content">
            <form action="{{ route('departments.update', $department->id) }}" method="post">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="col-md-6">
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">{{ __('department.edit') }}</h3>
                            </div>
                            <div class="card-body">
                                {{-- Name --}}
                                <div class="form-group required">
                                    <label class="control-label" for="name">{{ __('department.name') }}</label>
                                    <input type="text" name="name" id="name" value="{{ old('name', $department->name) }}"
                                        class="form-control {!! $errors->has('name') ? 'error' : '' !!}">
                                    @if ($errors->has('name'))
                                        <span class="text-danger">{{ $errors->first('name') }}</span>
                                    @endif
                                </div>

                                {{-- Description --}}
                                <div class="form-group">
                                    <label class="control-label" for="description">{{ __('department.description') }}</label>
                                    <textarea name="description" id="description" rows="4" class="form-control">{{ old('description', $department->description) }}</textarea>
                                    @if ($errors->has('description'))
                                        <span class="text-danger">{{ $errors->first('description') }}</span>
                                    @endif
                                </div>

                                {{-- Manager --}}
                                <div class="form-group">
                                    <label for="manager_id">{{ __('department.manager') }}</label>
                                    <select class="select2" style="width: 100%;" name="manager_id" id="manager_id">
                                        <option value="">{{ __('department.select_manager') }}</option>
                                        @foreach ($users as $user)
                                            <option value="{{ $user->id }}" {{ $user->id == old('manager_id', $user->isManager()) ? 'selected' : '' }}>
                                                {{ $user->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('manager_id'))
                                        <span class="text-danger">{{ $errors->first('manager_id') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-6">
                        <a href="{{ route('departments.index') }}" class="btn btn-secondary">{{ __('users.cancel') }}</a>
                        <button type="submit" class="btn btn-success float-right">{{ __('department.save') }}</button>
                    </div>
                </div>
            </form>
        </section>
    </div>
</div>
@stop
