@extends('layout.app')
@section('title')
    {{ __('department.list_department') }}
@endsection
@section('content')
<div class="wrapper">
    @include('layout.navbar')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>{{ __('department.list_department') }}</h1>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="card">
                <div class="card-header">
                    <a class="btn btn-primary" href="{{ route('departments.create') }}">{{ __('department.add_department') }}</a>
                </div>
                <div class="card-body p-0">
                    <table class="table table-striped projects">
                        <thead>
                            <tr>
                                <th style="width: 20%">{{ __('department.name') }}</th>
                                <th style="width: 40%">{{ __('department.description') }}</th>
                                <th style="width: 25%">{{ __('department.manager') }}</th>
                                <th style="width: 15%">{{ __('department.action') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($departments as $department)
                                @php /** @var App\Models\Department $department */ @endphp
                                <tr>
                                    <td>
                                        <span>{{ $department->name }}</span>
                                    </td>
                                    <td>
                                        <div style="max-height: 200px">{!! nl2br(e($department->description)) !!}</div>
                                    </td>
                                    <td>
                                        <span>{{ $department->manager_name }}</span>
                                    </td>
                                    <td class="project-actions">
                                        <a type="button" class="btn btn-info btn-sm" href="{{ route('departments.edit', $department->id) }}">
                                            <i class="fas fa-pencil-alt"></i> {{ __('department.edit') }}
                                        </a>
                                        <a type="button" class="btn btn-danger btn-sm"
                                            onclick="removeDepartment('{{ route('departments.destroy', $department->id) }}', '{{ __('department.confirm_delete') }}')">
                                            <i class="fas fa-trash"></i> {{ __('department.delete') }}
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card-footer d-flex justify-content-between">
                    <p style="font-size: 1.25rem" class="record">
                        @php
                            $current_page = $departments->toArray()['current_page'];
                            $per_page = $departments->toArray()['per_page'];
                            $total = $departments->toArray()['total'];

                            $first_item = ($current_page - 1) * $per_page + 1;
                            $last_item = $current_page * $per_page > $total ? $total : $current_page * $per_page;

                            echo('Page ' . $current_page . ': ' . $first_item . '~' . $last_item . ' / ' . $total . ' ');
                        @endphp
                        {{__('profile.department')}}
                    </p>
                    {!! $departments->appends(request()->except('page'))->render() !!}
                </div>
            </div>
        </section>
    </div>
</div>
@stop
