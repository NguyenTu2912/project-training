$.ajaxSetup({
    headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
    },
});

function removeUser(id, url, message) {
    if (confirm(message)) {
        $.ajax({
            type: "DELETE",
            dataType: "JSON",
            url: url,
            success: function (response) {
                if (response.error === false) {
                    toastr.success(response.message);
                    location.reload();
                } else {
                    toastr.error(response.message);
                }
            },
        });
    }
}

function removeDepartment(url, message) {
    console.log(url)
    if (confirm(message)) {
        $.ajax({
            type: "DELETE",
            dataType: "JSON",
            url: url,
            success: function (response) {
                if (response.error === false) {
                    toastr.success(response.message);
                    location.reload();
                } else {
                    toastr.error(response.message);
                }
            },
        });
    }
}

function resetPassword(email, url, message) {
    console.log(email, url);
    const form = new FormData();
    form.append("email", email);
    if (confirm(message)) {
        $.ajax({
            processData: false,
            contentType: false,
            url: '/reset/password',
            type: "POST",
            data: form,
            dataType: "JSON",
            success: function (response) {
                if (response.error === false) {
                    toastr.success(response.message);
                } else {
                    toastr.error(response.message);
                }
            },
        });
    }
}

$("#export_csv").click(function () {
    let request = window.location.search;
    $("#export_csv").attr(
        "href",
        window.location.origin + "/employees/export-csv" + request
    );
});
