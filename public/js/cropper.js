$.ajaxSetup({
    headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
    },
});

function dataURLtoFile(dataurl, filename) {
    var arr = dataurl.split(","),
        mime = arr[0].match(/:(.*?);/)[1],
        bstr = atob(arr[1]),
        n = bstr.length,
        u8arr = new Uint8Array(n);
    while (n--) {
        u8arr[n] = bstr.charCodeAt(n);
    }
    return new File([u8arr], filename, { type: mime });
}

$(document).ready(function () {
    console.log(1);
    $image_crop = $("#image_demo").croppie({
        enableExif: true,
        viewport: {
            width: 150,
            height: 150,
            type: "square",
        },
        boundary: {
            width: 300,
            height: 300,
        },
    });
    $("#preview").hide();

    $(".label").click(function () {
        $("#upload").val("");
        $("#preview").hide();
    });

    $("#upload").click(function () {
        $("#upload").val("");
        $("#preview").hide();
    });

    $("#upload").on("change", function () {
        console.log(1);
        var filename = $(this)[0].files[0].name;
        var extension = filename.substr(filename.lastIndexOf("."));
        var allowedExtensionsRegx = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
        var isAllowed = allowedExtensionsRegx.test(extension);
        if (isAllowed) {
            var reader = new FileReader();
            reader.onload = function (event) {
                $image_crop
                    .croppie("bind", {
                        url: event.target.result,
                    })
                    .then(function () {
                        console.log("jQuery bind complete");
                    });
            };
            reader.readAsDataURL(this.files[0]);
            $("#imageModel").modal("show");
            $(".close").click(function (e) {
                $("#imageModel").modal("hide");
                $("#upload").val("");
            });
            $("#btn-close").click(function (e) {
                $("#imageModel").modal("hide");
                $("#upload").val("");
            });
        } else {
            $("#imageModel").modal("hide");
            $("#upload").val("");
            $("div.alert").remove();
            toastr.error("Đổi ảnh thất bại");
        }
    });
    $(".crop_image").click(function (event) {
        $image_crop
            .croppie("result", {
                type: "canvas",
                size: "viewport",
            })
            .then(function (response) {
                var file = dataURLtoFile(response, "a.png");
                const form = new FormData();
                form.append("file", file);
                $.ajax({
                    processData: false,
                    contentType: false,
                    url: "/upload/store",
                    type: "POST",
                    data: form,
                    dataType: "JSON",
                    success: function (response) {
                        $("#imageModel").modal("hide");
                        $("#upload").val("");
                        if (response.error === false) {
                            $("#image-show").attr("src", response.url);
                            $("div.alert").remove();
                            toastr.success(response.message);
                        } else {
                            $("div.alert").remove();
                            toastr.error(response.message);
                        }
                    },
                });
            });
    });

    $(".admin_crop_image").click(function (event) {
        $image_crop
            .croppie("result", {
                type: "canvas",
                size: "viewport",
            })
            .then(function (response) {
                $("#imageModel").modal("hide");
                var file = dataURLtoFile(response, 'a.png');
                var src = URL.createObjectURL(file);
                $("#preview").show();
                $("#preview").attr('src', src);
                $("#image_base").attr("value", response);
            });
    });
});
