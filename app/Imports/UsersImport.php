<?php

namespace App\Imports;

use App\Models\Department;
use App\Models\User;
use App\Repositories\Contract\DepartmentRepositoryInterface;
use App\Repositories\Contract\EmployeeRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Validators\Failure;
use Illuminate\Support\Str;

class UsersImport implements ToModel, WithValidation, SkipsOnFailure, WithStartRow
{
    use Importable, SkipsErrors, SkipsFailures;

    private EmployeeRepositoryInterface $employeeRepository;
    private DepartmentRepositoryInterface $departmentRepository;

    /**
     * Init service
     */
    public function __construct(EmployeeRepositoryInterface $employeeRepository, DepartmentRepositoryInterface $departmentRepository)
    {
        $this->employeeRepository = $employeeRepository;
        $this->departmentRepository = $departmentRepository;
    }

    /**
     * Handle failure
     *
     * @param Failure ...$failures
     * @return mixed
     * @throws ValidationException
     */
    public function onFailure(Failure ...$failures): mixed
    {
        throw ValidationException::withMessages(collect($failures)->map->toArray()->all());
    }

    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        $fields = array(
            'name' => 0,
            'email' => 1,
            'birthday' => 2,
            'start_at' => 3,
            'status' => 4,
            'phone' => 5,
            'role' => 6,
            'department_id' => 7,
        );

        $row[$fields['birthday']] = Carbon::parse($row[2])->format('Y-m-d');
        $row[$fields['start_at']] = Carbon::parse($row[3])->format('Y-m-d');

        $statusMapping = [
            'dang-lam-viec' => User::STATUS_WORKING,
            'working' => User::STATUS_WORKING,
            'work' => User::STATUS_WORKING,
            'da-nghi-viec' => User::STATUS_RESIGN,
            'resign' => User::STATUS_RESIGN
        ];
        $statusIndex = $fields['status'];
        $row[$statusIndex] = $statusMapping[Str::slug($row[$statusIndex])] ?? null;

        $roleMapping = [
            'quan-tri-vien' => User::ROLE_ADMIN,
            'admin' => User::ROLE_ADMIN,
            'quan-ly-bo-phan' => User::ROLE_MANAGER,
            'manager' => User::ROLE_MANAGER,
            'nhan-vien' => User::ROLE_EMPLOYEE,
            'employee' => User::ROLE_EMPLOYEE,
        ];
        $roleIndex = $fields['role'];
        $row[$roleIndex] = $roleMapping[Str::slug($row[$roleIndex])] ?? null;

        $department = $this->departmentRepository->findByName((string)$row[$fields['department_id']]);
        $row[$fields['department_id']] = $department->id ?? null;

        $user = $this->employeeRepository->searchUserByEmail((string)$row[$fields['email']]);

        if ($user) {
            $data = [
                'name' => $row[0],
                'email' => $row[1],
                'birthday' => $row[2],
                'start_at' => $row[3],
                'status' => $row[4],
                'phone' => $row[5],
                'role' => $row[6],
                'department_id' => $row[7],
            ];

            $this->employeeRepository->update($user->id, $data);
        } else {
            User::create([
                'name' => $row[0],
                'email' => $row[1],
                'birthday' => $row[2],
                'start_at' => $row[3],
                'status' => $row[4],
                'phone' => $row[5],
                'role' => $row[6],
                'department_id' => $row[7],
                'password' => bcrypt(123456),
            ]);
        }
    }

    public function rules(): array
    {
        return [
            '0' => ['required', 'min:6', 'max:30'],
            '1' => ['required', 'email'],
            '2' => ['required', 'date', 'before:today'],
            '3' => ['required', 'date', 'before:today'],
            '4' => ['required', Rule::in('Đang làm việc', 'Đã nghỉ việc', 'Working', 'Resign')],
            '5' => ['min:10', 'max:10', 'regex:/(^([0-9]+)(\d+)?$)/u', 'nullable'],
            '6' => ['required', Rule::in('Quản trị viên',  'Quản lý bộ phận',  'Nhân viên',  'Admin',  'Manager',  'Employee')],
            '7' => ['required', Rule::exists('departments', 'name')],
        ];
    }

    /**
     * @return array
     */
    public function customValidationMessages()
    {
        return [
            '0.required' => __('import.name.required'),
            '1.required' => __('import.email.required'),
            '2.before' => __('import.birth_day.before'),
            '2.required' => __('import.birth_day.required'),
            '2.date' => __('import.birth_day.date'),
            '3.before' => __('import.start_at.before'),
            '3.required' => __('import.start_at.required'),
            '3.date' => __('import.start_at.date'),
            '4.required' => __('import.status.required'),
            '4.in' => __('import.status.in'),
            '5.min' => __('import.phone.min'),
            '5.max' => __('import.phone.max'),
            '5.regex' => __('import.phone.regex'),
            '6.required' => __('import.role.required'),
            '6.in' => __('import.role.in'),
            '7.required' => __('import.department.required'),
            '7.exists' => __('import.department.exists'),
        ];
    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return 2;
    }
}
