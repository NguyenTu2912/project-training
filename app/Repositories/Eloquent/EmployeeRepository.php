<?php

namespace App\Repositories\Eloquent;

use App\Helpers\CommonHelper;
use App\Models\User;
use App\Repositories\Contract\EmployeeRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;

class EmployeeRepository extends BaseRepository implements EmployeeRepositoryInterface
{
    /**
     * Get model name
     *
     * @return string
     */
    public function getModel(): string
    {
        return User::class;
    }

    /**
     * Search list employee
     *
     * @param array $filter
     * @param string $type {'GET_ALL', 'PAGINATION'}
     * @return LengthAwarePaginator|Collection
     */
    public function search(array $filter, string $type): LengthAwarePaginator|Collection
    {
        $loggedUser = Auth::user();

        $query = $this->model->sortable()
            ->with(['department'])
            ->when($loggedUser->isManager(), function ($q) use ($loggedUser) {
                $q->where('department_id', '=', $loggedUser->department_id);
            })
            ->when(strlen($departmentId = $filter['department_id'] ?? null), function ($q) use ($departmentId) {
                $q->where('department_id', '=', $departmentId);
            })
            ->when(strlen($status = $filter['status'] ?? null), function ($q) use ($status) {
                $q->where('status', '=', $status);
            })
            ->when(strlen($keyword = $filter['keyword'] ?? null), function ($q) use ($keyword) {
                $q->where('name', 'like', '%' . $keyword . '%');
            });

        return $type === config('common.TYPE_PAGINATION')
            ? $query->paginate(CommonHelper::retrievePageLimit($filter['limit'] ?? null))
            : $query->get();
    }

    /**
     * Search user by email
     *
     * @param string $email
     * @return User|Model|null
     */
    public function searchUserByEmail(string $email): User|Model|null
    {
        return $this->model->newQuery()
            ->where('email', $email)
            ->first();
    }

    /**
     * Get list managers
     *
     * @return Collection
     */
    public function listManagers(): Collection
    {
        return $this->model->newQuery()
            ->where('auth', User::ROLE_MANAGER)
            ->get();
    }

    /**
     * Get list employees
     *
     * @return Collection
     */
    public function listEmployees(): Collection
    {
        return $this->model->newQuery()
            ->where('auth', User::ROLE_EMPLOYEE)
            ->get();
    }

    /**
     * Get employee in department
     *
     * @param int $id
     * @return User|null
     */
    public function listEmployeesInDepartment(int $id) {
        return $this->model->newQuery()
            ->where('department_id', $id)
            ->get();
    }

    /**
     * Get manager in department
     *
     * @param int $departmentId
     * @return User|Model|null
     */
    public function getManagerDepartment(int $departmentId): Model|User|null
    {
        return $this->model->newQuery()
            ->where('department_id', $departmentId)
            ->where('role', User::ROLE_MANAGER)
            ->first();
    }
}
