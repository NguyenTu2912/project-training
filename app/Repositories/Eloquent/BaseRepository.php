<?php

namespace App\Repositories\Eloquent;

use App\Repositories\Contract\BaseRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

abstract class BaseRepository implements BaseRepositoryInterface
{
    /** @var Model */
    protected $model;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->setModel();
    }

    /**
     * Get model name
     *
     * @return string
     */
    abstract public function getModel(): string;

    /**
     * Set model
     */
    public function setModel()
    {
        $this->model = app()->make($this->getModel());
    }

    /**
     * Get all resources
     *
     * @return Collection
     */
    public function getAll(): Collection
    {
        return $this->model->all();
    }

    /**
     * Create new resource
     *
     * @param array $data
     * @return Model|null
     */
    public function create(array $data)
    {
        return $this->model->newQuery()->create($data);
    }

    /**
     * Find resource by id
     * 
     * @param int $id
     * @return Model|null
     */
    public function find(int $id)
    {
        return $this->model->newQuery()
            ->where('id', $id)
            ->first();
    }

    /**
     * Update a resource
     * 
     * @param int $id
     * @param array $data
     * @return bool
     */
    public function update(int $id, array $data): bool
    {
        return (bool) $this->model->newQuery()
            ->where('id', $id)
            ->update($data);
    }

    /**
     * Delete a resource
     *
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool
    {
        return (bool) $this->model->newQuery()
            ->where('id', $id)
            ->delete();
    }
}
