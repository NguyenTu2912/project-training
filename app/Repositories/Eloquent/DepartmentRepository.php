<?php

namespace App\Repositories\Eloquent;

use App\Helpers\CommonHelper;
use App\Models\Department;
use App\Models\User;
use App\Repositories\Contract\DepartmentRepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Pagination\LengthAwarePaginator;

class DepartmentRepository extends BaseRepository implements DepartmentRepositoryInterface
{
    /**
     * Get model name
     *
     * @return string
     */
    public function getModel(): string
    {
        return Department::class;
    }

    /**
     * Search list department
     *
     * @param array $filter
     * @return LengthAwarePaginator
     */
    public function search(array $filter = []): LengthAwarePaginator
    {
        return $this->model->newQuery()
            ->leftJoin('users', function (Builder $q) {
                $q->on('users.department_id', '=', 'departments.id')
                    ->where('users.role', '=', User::ROLE_MANAGER);
            })
            ->groupBy('departments.id')
            ->select(['departments.*', 'users.name AS manager_name'])
            ->paginate(CommonHelper::retrievePageLimit($filter['limit'] ?? null));
    }

    /**
     * Check and delete a department
     *
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool
    {
        $totalUser = User::query()->where('department_id', $id)->count();

        return $totalUser === 0 && parent::delete($id);
    }

    /**
     * Find department by name
     * 
     * @param string $name
     * @return Department|Model|null
     */
    public function findByName(string $name): Department|Model|null
    {
        return $this->model->newQuery()
            ->where('name', $name)
            ->first();
    }
}
