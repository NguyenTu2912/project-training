<?php

namespace App\Repositories\Contract;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

interface BaseRepositoryInterface
{
    /**
     * Get all resources
     *
     * @return mixed
     */
    public function getAll(): Collection;

    /**
     * Create new resource
     *
     * @param array $data
     * @return Model|null
     */
    public function create(array $data);

    /**
     * Find resource by id
     *
     * @param int $id
     * @return Model|null
     */
    public function find(int $id);

    /**
     * Update resource by id
     *
     * @param int $id
     * @param array $data
     * @return bool
     */
    public function update(int $id, array $data): bool;

    /**
     * Delete resource by id
     *
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool;
}
