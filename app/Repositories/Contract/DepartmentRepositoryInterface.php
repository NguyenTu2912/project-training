<?php

namespace App\Repositories\Contract;

use App\Models\Department;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

interface DepartmentRepositoryInterface extends BaseRepositoryInterface
{
    public function search(array $filter = []): LengthAwarePaginator;
    public function findByName(string $name): Department|Model|null;
}
