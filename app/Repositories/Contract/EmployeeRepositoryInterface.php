<?php

namespace App\Repositories\Contract;

use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

interface EmployeeRepositoryInterface extends BaseRepositoryInterface
{
    public function search(array $filter, string $type): LengthAwarePaginator|Collection;
    public function searchUserByEmail(string $email): User|Model|null;
    public function listManagers(): Collection;
    public function listEmployees(): Collection;
    public function listEmployeesInDepartment(int $id);
    public function getManagerDepartment(int $departmentId): User|Model|null;
}
