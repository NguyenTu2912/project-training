<?php

namespace App\Services;

use App\Repositories\Contract\BaseRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class BaseService
{
    /** @var BaseRepositoryInterface */
    protected $repository;

    /**
     * Handle delete a resource
     *
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool
    {
        return $this->repository->delete($id);
    }

    /**
     * Get all resources
     *
     * @return Collection
     */
    public function getAll(): Collection
    {
        return $this->repository->getAll();
    }

    /**
     * Update a resource
     *
     * @param array $data
     * @return Model|null
     */
    public function create(array $data)
    {
        return $this->repository->create($data);
    }

    /**
     * Update a resource
     *
     * @param int $id
     * @param array $data
     * @return bool
     */
    public function update(int $id, array $data): bool
    {
        return $this->repository->update($id, $data);
    }
}
