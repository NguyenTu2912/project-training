<?php

namespace App\Services;

use App\Helpers\DataType;
use App\Models\User;
use App\Repositories\Contract\DepartmentRepositoryInterface;
use App\Repositories\Contract\EmployeeRepositoryInterface;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Support\Facades\Log;

class DepartmentService extends BaseService
{
    /** @var DepartmentRepositoryInterface */
    protected $repository;
    /** @var EmployeeRepositoryInterface */
    protected EmployeeRepositoryInterface $employeeRepository;

    /**
     * Init service
     */
    public function __construct(
        DepartmentRepositoryInterface $departmentRepository,
        EmployeeRepositoryInterface $employeeRepository
    ) {
        $this->repository = $departmentRepository;
        $this->employeeRepository = $employeeRepository;
    }

    /**
     * Get list departments
     *
     * @param array $filter
     * @return LengthAwarePaginator
     */
    public function search(array $filter = []): LengthAwarePaginator
    {
        return $this->repository->search($filter);
    }

    /**
     * Update a department
     *
     * @param int $id
     * @param array $data
     * @return bool
     */
    public function update(int $id, array $data): bool
    {
        $newManagerId = DataType::int($data['manager_id'] ?? null);
        $oldManager = $this->employeeRepository->getManagerDepartment($id);

        DB::beginTransaction();
        try {
            if (($oldManager->id ?? null) !== $newManagerId) {
                // Update old manager's role to employee
                $oldManager && $this->employeeRepository->update($oldManager->id, ['role' => User::ROLE_EMPLOYEE]);

                // Update selected user's role to manager
                $newManagerId && $this->employeeRepository->update($newManagerId, ['role' => User::ROLE_MANAGER, 'department_id' => $id]);
            }

            // Update department data
            $this->repository->update($id, Arr::only($data, ['name', 'description']));

            DB::commit();
            return true;
        } catch (Exception $ex) {
            DB::rollBack();
            Log::error($ex);
            return false;
        }
    }
}
