<?php

namespace App\Services;

use App\Helpers\LogHelper;
use App\Models\User;
use App\Repositories\Contract\EmployeeRepositoryInterface;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class EmployeeService extends BaseService
{
    /** @var EmployeeRepositoryInterface */
    protected $repository;

    /**
     * Init service
     *
     * @param EmployeeRepositoryInterface $employeeRepository
     */
    public function __construct(EmployeeRepositoryInterface $employeeRepository)
    {
        $this->repository = $employeeRepository;
    }

    public function add($request)
    {
        $user = new User();
        $user->name = (string)$request->input('name');
        $user->email = (string)$request->input('email');
        $user->department_id = (int)$request->input('department');
        $user->birthday = (string)$request->input('birthday');
        $user->start_at = (string)$request->input('start_at');
        $user->status = (int)$request->input('status');
        $user->phone = (string)$request->input('phone');
        $user->password = bcrypt(123456);
        if ($request->image_base) {
            $image_parts = explode(";base64,", $request->image_base);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type = $image_type_aux[1];
            $image_base64 = base64_decode($image_parts[1]);
            $user_id = $user->id;
            $imageName = time() . '-' . $user_id . '.' . $image_type;

            file_put_contents(public_path('images'). '/' . $imageName, $image_base64);
            
            $user->avatar = '/images/' . $imageName;
        } else {
            $user->avatar = null;
        }
        $user->save();

        return true;
    }
    public function edit($request, User $user)
    {
        if ($user->isManager() && $user->department_id != (int) $request->input('department')){
            $user->role = User::ROLE_EMPLOYEE;
        }
        $user->name = (string)$request->input('name');
        $user->department_id = (int)$request->input('department');
        $user->birthday = (string)$request->input('birthday');
        $user->start_at = (string)$request->input('start_at');
        $user->status = (int)$request->input('status');
        $user->phone = (string)$request->input('phone');
        $user->save();
        if ($request->image_base) {

            $image_parts = explode(";base64,", $request->image_base);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type = $image_type_aux[1];
            $image_base64 = base64_decode($image_parts[1]);
            $user_id = $user->id;

            $imageName = time() . '-' . $user_id . '.' . $image_type;

            file_put_contents(public_path('images'). '/' . $imageName, $image_base64);

            DB::table('users')
                ->where('id', $user_id)
                ->update(['avatar' => '/images/' .  $imageName]);
        }
        return true;
    }

    /**
     * Handle change avatar
     *
     * @param Request $request
     * @return bool|string
     */
    public function changeAvatar(Request $request): bool|string
    {
        if ($request->hasFile('file')) {
            try {
                $request->validate(['file' => 'required|image|mimes:png,jpg,jpeg|max:2048']);

                $name = time() . '.' . $request->file->extension();
                $request->file->move(public_path('images'), $name);

                $avatar = "/images/{$name}";
                $this->repository->update(Auth::id(), ['avatar' => $avatar]);

                return $avatar;
            } catch (Exception $ex) {
                LogHelper::error($ex);
                return false;
            }
        }

        return false;
    }

    /**
     * Get list managers
     *
     * @return Collection
     */
    public function listManagers(): Collection
    {
        return $this->repository->listManagers();
    }

    /**
     * Get list employees
     *
     * @return Collection
     */
    public function listEmployees(): Collection
    {
        return $this->repository->listEmployees();
    }

    /**
     * Search list employee
     *
     * @param array $filter
     * @return LengthAwarePaginator
     */
    public function search(array $filter = []): LengthAwarePaginator
    {
        return $this->repository->search($filter, config('common.TYPE_PAGINATION'));
    }

    /**
     * Search employee by email
     *
     * @param string $email
     * @return User|null
     */
    public function searchUserByEmail(string $email): ?User
    {
        return $this->repository->searchUserByEmail($email);
    }

    /**
     * Get employee in department
     *
     * @param int $id
     * @return User|null
     */
    public function listEmployeesInDepartment(int $id) {
        return $this->repository->listEmployeesInDepartment($id);
    }

    /**
     * Get manager in department
     *
     * @param int $id
     * @return User|null
     */
    public function getManagerDepartment(int $id) {
        return $this->repository->getManagerDepartment($id);
    }
}
