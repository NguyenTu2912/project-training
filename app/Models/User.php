<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Kyslik\ColumnSortable\Sortable;
use Laravel\Sanctum\HasApiTokens;

/**
 * User model
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $birthday
 * @property string $start_at
 * @property int $status
 * @property int $role
 * @property string|null $avatar
 * @property string|null $phone
 * @property int $department_id
 * @property int $is_first_login
 */
class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, Sortable;

    /* USER ROLE */
    const ROLE_ADMIN = 1;
    const ROLE_MANAGER = 2;
    const ROLE_EMPLOYEE = 3;

    /* WORKING STATUS */
    const STATUS_WORKING = 1;
    const STATUS_RESIGN = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'birthday',
        'start_at',
        'status',
        'role',
        'avatar',
        'phone',
        'department_id',
        'is_first_login',
    ];

    public $sortable = ['birthday', 'start_at'];
    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Relationship N User - 1 Department
     *
     * @return BelongsTo
     */
    public function department(): BelongsTo
    {
        return $this->belongsTo(Department::class, 'department_id', 'id');
    }

    /**
     * Get avatar link
     *
     * @return string
     */
    public function getAvatarLink(): string
    {
        return asset($this->avatar ?? 'images/default.jpeg');
    }

    /**
     * Check the user's role is administrator
     *
     * @return bool
     */
    public function isAdmin(): bool
    {
        return $this->role === self::ROLE_ADMIN;
    }

    /**
     * Check the user's role is manager
     *
     * @return bool
     */
    public function isManager(): bool
    {
        return $this->role === self::ROLE_MANAGER;
    }

    /**
     * Check the user's status is working
     *
     * @return bool
     */
    public function isWorking(): bool
    {
        return $this->status === self::STATUS_WORKING;
    }
}
