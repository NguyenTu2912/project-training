<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rule;

class DepartmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        $department = Route::input('department');

        $ruleUniqueName = Rule::unique('departments', 'name');
        $ruleExistManager = Rule::exists('users', 'id');
        if ($department) {
            $ruleExistManager->where('department_id', $department->id);
            $ruleUniqueName->whereNot('id', $department->id);
        }

        return [
            'name' => "required|string|min:3|max:50|{$ruleUniqueName}",
            'description' => 'nullable|string|max:255',
            'manager_id' => "nullable|integer|{$ruleExistManager}",
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'name.required' => __('messages.department_name_required'),
            'name.min' => __('messages.department_name_min'),
            'name.max' => __('messages.department_name_min'),
            'name.unique' => __('messages.department_name_unique'),
            'description.max' => __('messages.description_max'),
        ];
    }

    /**
     * Set flash session when validate failed
     *
     * @param $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if (! $this->isMethod('GET')) { 
            if ($validator->errors()->all()) {
                Session::flash('error', __('messages.fail'));
            }
        }     
    }
}
