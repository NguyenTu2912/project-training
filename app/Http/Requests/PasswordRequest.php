<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class PasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        if ($this->isMethod('GET')) {
            return [];
        }

        return [
            'current_password' => 'required',
            'new_password' => 'required|min:6|max:30|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/',
            'confirm_password' => 'required|same:new_password',
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'current_password.required' => __('messages.current_password_required'),
            'new_password.required' => __('messages.new_password_required'),
            'confirm_password.required' => __('messages.confirm_password_required'),
            'new_password.min' => __('messages.new_password_min'),
            'new_password.max' => __('messages.new_password_max'),
            'confirm_password.same' => __('messages.confirm_password_same'),
            'new_password.regex' => __('messages.new_password_regex'),
        ];
    }

    /**
     * Set flash session when validate failed
     *
     * @param $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if (! $this->isMethod('GET')) {
            $validator->after(function ($validator) {
                $loggedUser = Auth::user();
                if (! Hash::check($this->get('current_password'), $loggedUser->password)) {
                    $validator->errors()->add('current_password', __('messages.wrong_password'));
                }

                if (Hash::check($this->get('new_password'), $loggedUser->password)) {
                    $validator->errors()->add('new_password', __('messages.duplicate_password'));
                }
            });
            if ($validator->errors()->all()) {
                Session::flash('error', __('messages.change_password_failed'));
            }
        }
    }
}
