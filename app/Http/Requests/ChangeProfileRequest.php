<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Session;

class ChangeProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        if ($this->isMethod('GET')) {
            return [];
        }

        return [
            'birthday' => 'required|before:today',
            'phone' => 'nullable|min:10|max:10|regex:/(^([0-9]+)(\d+)?$)/u',
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'birthday.required' => __('messages.birth_day_required'),
            'phone.min' => __('messages.phone_min'),
            'phone.max' => __('messages.phone_max'),
            'phone.regex' => __('messages.phone_regex'),
            'birthday.before' => __('messages.birth_day_before'),
        ];
    }

    /**
     * Set flash session when validate failed
     *
     * @param $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if (! $this->isMethod('GET')) {
            if ($validator->errors()->all()) {
                Session::flash('error', __('messages.change_profile_failed'));
            }
        }
    }
}
