<?php

namespace App\Http\Requests;

use App\Helpers\DropdownHelper;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rule;

class EditEmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        $workingStatuses = DropdownHelper::getWorkingStatus();

        return [
            'name' => 'required|string|min:6|max:30',
            'department' => 'required|int|' . Rule::exists('departments', 'id'),
            'birthday' => 'required|date|before:today',
            'start_at' => 'required|date|before:today',
            'status' => 'required|' . Rule::in(array_keys($workingStatuses)),
            'phone' => 'nullable|min:10|max:10|regex:/(^([0-9]+)(\d+)?$)/u',
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'name.required' => __('messages.name_required'),
            'name.min' => __('messages.name_min'),
            'name.max' => __('messages.name_max'),
            'department.required' => __('messages.department_required'),
            'birthday.required' => __('messages.birth_day_required'),
            'start_at.required' => __('messages.start_at_required'),
            'phone.min' => __('messages.phone_min'),
            'phone.max' => __('messages.phone_max'),
            'phone.regex' => __('messages.phone_regex'),
            'birthday.before' => __('messages.birth_day_before'),
            'start_at.before' => __('messages.start_at_before'),
            'status.required' => __('messages.status_required'),
        ];
    }

    /**
     * Set flash session when validate failed
     *
     * @param $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if (! $this->isMethod('GET')) {
            if ($validator->errors()->all()) {
                Session::flash('error', __('messages.edit_user_fail'));
            }
        }
    }
}
