<?php

namespace App\Http\Controllers;

use App\Http\Requests\ChangeProfileRequest;
use App\Http\Requests\PasswordRequest;
use App\Services\EmployeeService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class ProfileController extends Controller
{
    protected EmployeeService $employeeService;

    /**
     * Constructor
     *
     * @param EmployeeService $employeeService
     */
    public function __construct(EmployeeService $employeeService)
    {
        $this->employeeService = $employeeService;
    }

    /**
     * Display view profile detail
     *
     * @return View
     */
    public function show(): View
    {
        return view('employees.profile.detail', ['user' => Auth::user()]);
    }

    /**
     * Display view edit profile
     *
     * @return View
     */
    public function edit(): View
    {
        return view('employees.profile.edit', ['user' => Auth::user()]);
    }

    /**
     * Display and handle submit edit profile form
     *
     * @param ChangeProfileRequest $request
     * @return RedirectResponse
     */
    public function update(ChangeProfileRequest $request): RedirectResponse
    {
        $this->employeeService->update(Auth::id(), $request->only(['birthday', 'phone']));

        return redirect('profile')->with('success', __('messages.change_profile_success'));
    }

    /**
     * Display and handle submit change password form
     *
     * @param PasswordRequest $request
     * @return View|RedirectResponse
     */
    public function changePassword(PasswordRequest $request): View|RedirectResponse
    {
        if ($request->isMethod('GET')) {
            return view('changePassword');
        }

        $this->employeeService->update(Auth::id(), [
            'password' => bcrypt($request->get('new_password')),
            'is_first_login' => config('common.IS_NOT_FIRST_LOGIN'),
        ]);
        return redirect('profile')->with('success', __('messages.change_password_success'));
    }

    /**
     * Change avatar
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function changeAvatar(Request $request): JsonResponse
    {
        $url = $this->employeeService->changeAvatar($request);
        if ($url !== false) {
            return response()->json([
                'error' => false,
                'url' => $url,
                'message' => __('messages.upload_success'),
            ]);
        }

        return response()->json([
            'error' => true,
            'message' => __('messages.upload_failed'),
        ]);
    }
}
