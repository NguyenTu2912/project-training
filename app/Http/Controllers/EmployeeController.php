<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateEmployeeRequest;
use App\Services\EmployeeService;
use App\Models\User;
use Exception;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\UsersExport;
use App\Http\Requests\EditEmployeeRequest;
use App\Imports\UsersImport;
use App\Services\DepartmentService;
use Illuminate\Support\Facades\Session;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class EmployeeController extends Controller
{
    private DepartmentService $departmentService;
    private EmployeeService $employeeService;

    /**
     * Constructor
     *
     * @param EmployeeService $employeeService
     * @param DepartmentService $departmentService
     */
    public function __construct(EmployeeService $employeeService, DepartmentService $departmentService)
    {
        $this->employeeService = $employeeService;
        $this->departmentService = $departmentService;
    }

    /**
     * Display list employee view
     *
     * @return View
     */
    public function index(): View
    {
        $departments = $this->departmentService->getAll();
        $users = $this->employeeService->search();

        return view('employees.list', compact('users', 'departments'));
    }

    /**
     * Search list employee
     *
     * @param Request $request
     * @return View
     */
    public function search(Request $request): View
    {
        $departments = $this->departmentService->getAll();
        $users = $this->employeeService->search($request->all());

        return view('employees.list', compact('users', 'departments'));
    }

    /**
     * Delete an employee
     *
     * @param User $user
     * @return JsonResponse
     */
    public function destroy(User $user): JsonResponse
    {
        $result = false;
        if (Auth::id() !== $user->id) {
            $result = $this->employeeService->delete($user->id);
        }

        return $result
            ? response()->json(['error' => false, 'message' => __('messages.delete_user_success')])
            : response()->json(['error' => true, 'message' => __('messages.delete_user_fail')]);
    }

    /**
     * Display view add new employee
     *
     * @return View
     */
    public function create(): View
    {
        $departments = $this->departmentService->getAll();

        return view('employees.add')->with('departments', $departments);
    }

    /**
     * Handle insert new employee
     *
     * @param CreateEmployeeRequest $request
     * @return RedirectResponse
     */
    public function store(CreateEmployeeRequest $request): RedirectResponse
    {
        try {
            $this->employeeService->add($request);
            return redirect('employees')->with('success', __('messages.insert_user_success'));
        } catch (Exception $ex) {
            return redirect('employees/add')->with('error', __('messages.insert_user_fail'));
        }
    }

    /**
     * Display view user profile
     *
     * @param User $user
     * @return View
     */
    public function show(User $user): View
    {
        // Manager view only employees in his/her department
        $loggedUser = Auth::user();
        if ($loggedUser->isManager() && $loggedUser->department_id !== $user->department_id) {
            abort(404);
        }

        return view('employees.profile.detail', compact('user'));
    }

    /**
     * Display view edit employee
     *
     * @param User $user
     * @return View
     */
    public function edit(User $user): View
    {
        $departments = $this->departmentService->getAll();

        return view('employees.edit', compact('user', 'departments'));
    }

    /**
     * Update an employee
     *
     * @param User $user
     * @param EditEmployeeRequest $request
     * @return RedirectResponse
     */
    public function update(User $user, EditEmployeeRequest $request): RedirectResponse
    {
        $this->employeeService->edit($request, $user);

        Session::flash('success', __('messages.edit_user_success'));
        return redirect()->route('employees.index');
    }

    /**
     * Export list users to Excel file
     *
     * @param Request $request
     * @return BinaryFileResponse
     * @throws BindingResolutionException
     */
    public function exportCsv(Request $request): BinaryFileResponse
    {
        $usersExport = new UsersExport($request->all());
        
        Session::flash('success', __('messages.export_success'));
        return Excel::download($usersExport, 'users.xlsx');
    }

    /**
     * Display view import users
     *
     * @return View
     */
    public function import(): View
    {
        return view('employees.import');
    }

    /**
     * Handle import users by CSV file
     *
     * @param Request $request
     * @param UsersImport $usersImport
     * @return RedirectResponse
     */
    public function importCsv(Request $request, UsersImport $usersImport): RedirectResponse
    {
        if ($request->file('file')) {
            Excel::import($usersImport, $request->file('file'));
            return redirect()->route('employees.index')->with('success', __('messages.import_success'));
        }

        // File does not exist: back to previous page
        return redirect()->back()->with('error', __('messages.import_failed'));
    }
}
