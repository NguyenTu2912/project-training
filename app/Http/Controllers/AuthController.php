<?php

namespace App\Http\Controllers;

use App\Jobs\SendEmail;
use App\Services\EmployeeService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Illuminate\Http\JsonResponse;

class AuthController extends Controller
{
    private EmployeeService $employeeService;

    /**
     * Constructor
     *
     * @param EmployeeService $employeeService
     */
    public function __construct(EmployeeService $employeeService)
    {
        $this->employeeService = $employeeService;
    }

    /**
     * Display and handle submit login form
     *
     * @param Request $request
     * @return View|RedirectResponse
     */
    public function login(Request $request): View|RedirectResponse
    {
        if ($request->isMethod('GET')) {
            if (Auth::check()) {
                return redirect()->route('profile');
            }
            return view('login');
        }

        /* Handle submit login form */
        $request->validate(['email' => 'required|email', 'password' => 'required']);
        $flgRemember = $request->get('remember') === '1';
        if (Auth::attempt($request->only('email', 'password'), $flgRemember)) {
            Session::flash('success', __('messages.login_success'));
            return redirect()->route('profile');
        }

        Session::flash('error', __('messages.login_failed'));
        return redirect()->back();
    }

    /**
     * Handle logout
     *
     * @return RedirectResponse
     */
    public function logout(): RedirectResponse
    {
        Auth::logout();
        return redirect()->route('login');
    }

    /**
     * Display and handle submit forgot password form
     *
     * @param Request $request
     * @return View|RedirectResponse|JsonResponse
     */
    public function forgotPassword(Request $request): View|RedirectResponse|JsonResponse
    {
        if ($request->isMethod('GET')) {
            return view('forgotPassword');
        }

        $request->validate(['email' => 'required|email']);

        $user = $this->employeeService->searchUserByEmail($request->email);
        if ($user) {
            $password = Str::random(10);
            $listUser = array();
            $messages = array();
            $user->is_first_login = config('common.IS_FIRST_LOGIN');
            $user->password = bcrypt($password);
            $user->save();
            $message = [
                'title' => __('messages.reset_password_title'),
                'task' => __('messages.reset_password_task'),
                'data' => $password,
            ];
            $listUser[] = $user;
            $messages[] = $message;

            $result = $this->dispatch(new SendEmail($messages, $listUser));
            if (Auth::user()) {
                return $result
                    ? response()->json(['error' => false, 'message' => __('messages.reset_success')])
                    : response()->json(['error' => true, 'message' => __('messages.fail')]);
            } else {
                Session::flash('success', __('messages.reset_success'));
                return redirect()->route('login');
            }
        } else {
            return response()->json(['error' => true, 'message' => __('messages.fail')]);
        }
    }

    /**
     * Handle change language
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function changeLanguage(Request $request): RedirectResponse
    {
        App::setLocale($request->get('lang'));
        session()->put('locale', $request->get('lang'));

        return redirect()->back();
    }
}
