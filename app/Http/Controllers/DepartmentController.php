<?php

namespace App\Http\Controllers;

use App\Http\Requests\DepartmentRequest;
use App\Services\DepartmentService;
use App\Models\Department;
use App\Services\EmployeeService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class DepartmentController extends BaseController
{
    private DepartmentService $departmentService;
    private EmployeeService $employeeService;

    /**
     * Constructor
     *
     * @param DepartmentService $departmentService
     * @param EmployeeService $employeeService
     */
    public function __construct(DepartmentService $departmentService, EmployeeService $employeeService)
    {
        $this->departmentService = $departmentService;
        $this->employeeService = $employeeService;
    }

    /**
     * Display view department list
     *
     * @return View
     */
    public function index(): View
    {
        $departments = $this->departmentService->search();

        return view('departments.index', compact('departments'));
    }

    /**
     * Display view create a department
     *
     * @return View
     */
    public function create(): View
    {
        return view('departments.create');
    }

    /**
     * Create new department
     *
     * @param DepartmentRequest $request
     * @return RedirectResponse
     */
    public function store(DepartmentRequest $request): RedirectResponse
    {
        $result = $this->departmentService->create($request->only(['name', 'description']));

        return $result
            ? redirect()->route('departments.index')->with('success', __('messages.insert_department_success'))
            : redirect()->route('departments.index')->with('error', __('messages.insert_department_fail'));
    }

    /**
     * Display view edit a department
     *
     * @param Department $department
     * @return View
     */
    public function edit(Department $department): View
    {
        $users = $this->employeeService->listEmployeesInDepartment($department->id);

        return view('departments.edit', compact('department', 'users'));
    }

    /**
     * Update a department
     *
     * @param Department $department
     * @param DepartmentRequest $request
     * @return RedirectResponse
     */
    public function update(Department $department, DepartmentRequest $request): RedirectResponse
    {
        $this->departmentService->update($department->id, $request->all());

        return redirect()->route('departments.index')->with('success', __('messages.update_department_success'));
    }

    /**
     * Delete a department
     *
     * @param Department $department
     * @return JsonResponse
     */
    public function destroy(Department $department): JsonResponse
    {
        $result = $this->departmentService->delete($department->id);

        return $result
            ? $this->success(['message' => __('messages.delete_department_success')])
            : $this->error(['message' => __('messages.delete_department_fail')]);
    }
}
