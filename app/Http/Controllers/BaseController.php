<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;

class BaseController extends Controller
{
    /**
     * Response json successful
     *
     * @param array|null $data
     * @return JsonResponse
     */
    public function success(array $data = null): JsonResponse
    {
        $response = array_merge((array) $data, ['error' => false]);

        return response()->json($response);
    }

    /**
     * Response json error
     *
     * @param array|null $data
     * @return JsonResponse
     */
    public function error(array $data = null): JsonResponse
    {
        $response = array_merge((array) $data, ['error' => true]);

        return response()->json($response);
    }
}
