<?php

namespace App\Helpers;

use Exception;
use ReflectionMethod;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Log;

class LogHelper
{
    /**
     * Handle write custom log errors
     *
     * @param Exception $exception
     */
    public static function error(Exception $exception)
    {
        $aryTreeFolders = explode(DIRECTORY_SEPARATOR, __DIR__);
        $aryTreeFolders = array_slice($aryTreeFolders, 0, count($aryTreeFolders) - 2);
        $prefixPath = implode('/', $aryTreeFolders) . '/';
        $message = $exception->getMessage();
        $flgJsonEncode = JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES;

        try {
            $backtrace = debug_backtrace()[1];
            $reflection = new ReflectionMethod($backtrace['class'], $backtrace['function']);
            $context = [
                'File' => str_replace($prefixPath, '', $reflection->getFileName()),
                'Class' => $reflection->class,
                'Method' => $reflection->name,
                'Line' => $reflection->getStartLine() . ' ~ ' . $reflection->getEndLine(),
                'Exception' => str_replace($prefixPath, '', $exception->getFile()) . ':' . $exception->getLine(),
            ];
            if ($route = Route::getCurrentRoute()) {
                $context['Uri'] = $route->uri();
            }

            $content = json_encode($context, $flgJsonEncode);
            Log::error("{$message}\n{$content}\n");
        } catch (\Exception $ex) {
            Log::error($message, ['exception' => $exception]);
        }
    }
}
