<?php

namespace App\Helpers;

use App\Models\User;

class DropdownHelper
{
    /**
     * Get list user status dropdown
     *
     * @return array
     */
    public static function getUserRole(): array
    {
        return [
            User::ROLE_ADMIN => __('users.admin'),
            User::ROLE_MANAGER => __('users.manager'),
            User::ROLE_EMPLOYEE => __('users.employee'),
        ];
    }

    /**
     * Get list user status dropdown
     *
     * @return array
     */
    public static function getWorkingStatus(): array
    {
        return [
            User::STATUS_WORKING => __('users.work'),
            User::STATUS_RESIGN => __('users.resign'),
        ];
    }

    /**
     * Get list paginator limit dropdown
     *
     * @return array
     */
    public static function getPaginatorLimit(): array
    {
        return [
            10 => 10,
            20 => 20,
            50 => 50,
        ];
    }
}
