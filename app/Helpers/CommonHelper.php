<?php

namespace App\Helpers;

class CommonHelper
{
    /**
     * Get paginator limit
     *
     * @param mixed $limit
     * @return int
     */
    public static function retrievePageLimit(mixed $limit): int
    {
        $limits = DropdownHelper::getPaginatorLimit();
        return isset($limits[$limit]) ? $limit : array_key_first($limits);
    }
}
