<?php

namespace App\Helpers;

class DataType
{
    /**
     * Get variable type is integer
     * Check variable type is integer (accept string as integer), return the default value if false
     *
     * @param mixed $variable
     * @param null|mixed $default
     * @return int|mixed
     */
    public static function int(mixed $variable, mixed $default = null): mixed
    {
        return self::isInteger($variable) ? (int) $variable : $default;
    }

    /**
     * Get variable type is float
     *
     * @param mixed $variable
     * @param null|mixed $default
     * @return float|mixed
     */
    public static function float(mixed $variable, mixed $default = null): mixed
    {
        return filter_var($variable, FILTER_VALIDATE_FLOAT) !== false ? (float) $variable : $default;
    }

    /**
     * Get variable type is string
     * Check variable type is string (accept numeric as integer), return the default value if false
     *
     * @param mixed $variable
     * @param null|mixed $default
     * @return string|mixed
     */
    public static function string(mixed $variable, mixed $default = null): mixed
    {
        return is_string($variable) || is_numeric($variable) ? (string) $variable : $default;
    }

    /**
     * Get variable type is array
     * Check variable type is array, return the default value if false
     *
     * @param mixed $variable
     * @param null|mixed $default
     * @return array|mixed
     */
    public static function array(mixed $variable, mixed $default = null): mixed
    {
        return is_array($variable) ? $variable : $default;
    }

    /**
     * Check variable type is integer (accept string as integer)
     *
     * @param mixed $variable
     * @return bool
     */
    public static function isInteger(mixed $variable): bool
    {
        return filter_var($variable, FILTER_VALIDATE_INT) !== false;
    }
}
