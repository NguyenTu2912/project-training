<?php

namespace App\Exports;

use App\Repositories\Contract\EmployeeRepositoryInterface;
use Illuminate\Contracts\Container\BindingResolutionException;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;

class UsersExport implements FromView
{
    use Exportable;

    private EmployeeRepositoryInterface $repository;
    private array $filter;

    /**
     * Constructor
     *
     * @throws BindingResolutionException
     */
    public function __construct(array $filter)
    {
        $this->repository = app()->make(EmployeeRepositoryInterface::class);
        $this->filter = $filter;
    }

    /**
     * @return View
     */
    public function view(): View
    {
        $users = $this->repository->search($this->filter, config('common.TYPE_GET_ALL'));

        return view('employees.export', compact('users'));
    }
}
