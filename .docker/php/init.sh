#!/bin/bash

cd /var/www/html

# Copy .env from .env.dev when does not exist .env file
if [ ! -f .env ]; then
    cp .env.dev .env
fi

# Run composer install when does not exist vendor folder
if [ ! -d vendor ]; then
    composer install
fi

# Chmod storage folder and make symlink storage to public folder
chmod -R 777 storage
php artisan storage:link

# Run supervisor
supervisord -c /etc/supervisor/supervisord.conf

# Run crontab
service cron start && crontab /etc/cron.d/laravel-schedule

# Run migrate after 5 seconds (wait for the database is created)
sleep 5 && php artisan migrate

# Start default
/usr/local/bin/docker-php-entrypoint php-fpm
