<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\DepartmentController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});


Route::get('/login', [AuthController::class, 'login'])->name('login');
Route::post('/login', [AuthController::class, 'login'])->name('login');
Route::get('/forgot-password', [AuthController::class, 'forgotPassword'])->name('forgot_password');
Route::post('/forgot-password', [AuthController::class, 'forgotPassword'])->name('forgot_password');
Route::get('change-language', [AuthController::class, 'changeLanguage'])->name('changeLanguage');


Route::group(['middleware' => ['auth']], function () {
    Route::get('/logout', [AuthController::class, 'logout'])->name('logout');
    Route::get('/change_password', [ProfileController::class, 'changePassword'])->name('changePassword');
    Route::post('/change_password/store', [ProfileController::class, 'changePassword'])->name('changePasswordCustom');
    Route::group(['middleware' => ['check-first-login']], function () {
        Route::get('/profile', [ProfileController::class, 'show'])->name('profile');
        Route::get('/profile/edit', [ProfileController::class, 'edit'])->name('profile.edit');
        Route::post('/profile', [ProfileController::class, 'update'])->name('profile.update');

        Route::post('/upload/store', [ProfileController::class, 'changeAvatar']);
        Route::group(['middleware' => ['verified-account']], function () {
            Route::group(['prefix' => 'employees'], function () {
                Route::get('/', [EmployeeController::class, 'index'])->name('employees.index');
                Route::get('/search', [EmployeeController::class, 'search'])->name('search');
                Route::get('/export-csv', [EmployeeController::class, 'exportCsv']);
                Route::group(['middleware' => ['verified-admin']], function () {
                    Route::get('/create', [EmployeeController::class, 'create'])->name('employees.create');
                    Route::post('/', [EmployeeController::class, 'store'])->name('employees.store');
                    Route::get('/import', [EmployeeController::class, 'import'])->name('import');
                    Route::post('/import-csv', [EmployeeController::class, 'importCsv'])->name('import-csv');
                    Route::get('/{user}/edit', [EmployeeController::class, 'edit'])->name('employees.edit');
                    Route::put('/{user}', [EmployeeController::class, 'update'])->name('employees.update');
                    Route::delete('/{user}', [EmployeeController::class, 'destroy'])->name('employees.destroy');
                });

                Route::get('/{user}', [EmployeeController::class, 'show'])->name('employees.show');
            });
            Route::group(['middleware' => ['verified-admin'], 'prefix' => 'departments', 'as' => 'departments.'], function () {
                Route::get('/', [DepartmentController::class, 'index'])->name('index');
                Route::get('/create', [DepartmentController::class, 'create'])->name('create');
                Route::post('/', [DepartmentController::class, 'store'])->name('store');
                Route::get('/{department}/edit', [DepartmentController::class, 'edit'])->name('edit');
                Route::put('/{department}', [DepartmentController::class, 'update'])->name('update');
                Route::delete('/{department}', [DepartmentController::class, 'destroy'])->name('destroy');
            });
        });
        Route::group(['middleware' => ['verified-admin']], function () {
            Route::post('/reset/password', [AuthController::class, 'forgotPassword'])->name('resetPassword');
        });
    });
});
